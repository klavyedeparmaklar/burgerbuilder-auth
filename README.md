# This is just Redux tutorial

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

## Useful Resources & Links
- [ReactJS](https://reactjs.org/)
- [Redux](https://redux.js.org/)
  - [Redux Async Logic](https://redux.js.org/tutorials/fundamentals/part-6-async-logic)
- [redux-thunk](https://github.com/reduxjs/redux-thunk)
- [Redux DevTools](https://github.com/zalmoxisus/redux-devtools-extension)

### `npm install`
### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.
<br>
### Some Pictures about BurgerBuilder 
<br>
![BurgerBuilder](/src/assets/images/burger-builder.JPG)
<br>
![BurgerBuilderOrder](/src/assets/images/burger-builder-order.JPG)
<br>
![BurgerBuilderAddress](/src/assets/images/burger-builder-address.JPG)
<br>
![BurgerBuilderOrderList](/src/assets/images/burger-builder-order-list.JPG)

### Mobile and Sidedraw
<br>
&nbsp;&nbsp;&nbsp;<img src="/src/assets/images/burger-builder-responsive.JPG" width="35%" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="/src/assets/images/burger-builder-responsive-sidedraw.JPG" width="35%" />

<br>
![BurgerBuilderFirebase](/src/assets/images/burger-builder-firebase.JPG)
<br>

### For Tutorial  :pencil2:
- [React- The Complete Guide](https://www.udemy.com/course/react-the-complete-guide-incl-redux/):heavy_check_mark:
